**LUIZA LABS TEST**
____________________________________________________________________________________________________

Olá Pessoal, 

Vou explicar para vocês como subir a aplicação e mostrar os possíveis endpoints que temos para executar
os testes, mas antes de tudo essas são as tecnologias que foram utilizadas:

- JAVA
- SPRING (Spring Boot, Spring Security, Spring Data)
- MySQL
- Docker

____________________________________________________________________________________________________

Bom Vamos começar instalando as ferramentas para que possamos testar a aplicação, segue as instruções
de instalação abaixo:

**JAVA 13**

    Windows:
        https://www.oracle.com/java/technologies/javase/jdk13-archive-downloads.html
        Baixe o .EXE e seguir os passos de instalação

    Linux:
        https://www.oracle.com/java/technologies/javase/jdk13-archive-downloads.html
        .DEB - dpkg -i package_file.deb
        .RPM - rpm -i package_file.rpm
____________________________________________________________________________________________________

**DOCKER**
    
    Windows:
        https://hub.docker.com/editions/community/docker-ce-desktop-windows/
        Baixar o .EXE (Get Docker) seguir os passos de instalação e iniciar o docker for Windows

    Linux:
        https://docs.docker.com/engine/install/debian/ - Distros derivado do Debian (Ubuntu, Kali Linux...)

        https://docs.docker.com/engine/install/centos/ - Distros derivado do Fedora/Red Hat (centOS...)
        

    No caso do Linux precisamos instalar o Docker-compose separadamente:

        sudo curl -L "https://github.com/docker/compose/releases/download/1.26.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
            
        sudo chmod +x /usr/local/bin/docker-compose

        docker-compose --version

____________________________________________________________________________________________________

**POSTMAN**

    No site do postman existe a documentação detalhada para instalação em todos os SO
    https://learning.postman.com/docs/getting-started/installation-and-updates/

    Caso prefira tem a opção de utilizar o postman online

____________________________________________________________________________________________________

**IDE**
    
    Para testar a aplicação precisamos importar o projeto para uma IDE do mundo JAVA de sua preferencia (Eclipse, IntelliJ)
    
    Vou utilizar o Eclipse como referencia:
    
    - Para importar seleciona a Aba 
        File -> Import -> Maven -> Existing maven projects -> Browser (Selecione a pasta do projeto) -> Finish

    - Após importar o projeto -> botão direito no projeto -> Maven -> Update project -> Selecione o projeto e o Force update -> OK

____________________________________________________________________________________________________

**EXECUÇÃO DA APLICAÇÃO**

Após instalar todas as ferramentas necessárias, vamos começar os trabalhos para subir a aplicação.

Através de um terminal da sua preferência (PowerShell, gitBash) navegue até a pasta do projeto luizalabs,
dentro do projeto existe um arquivo chamado docker-compose.yml, ele está configurado para subir um container
com o MySQL, então basta digitar:
    
    docker-compose up -d

Se o comando funcionou sem problemas, já estamos com um container do MYSQL rodando, para visualizar digite o 
comando a seguir:

    docker ps

No campo status conseguimos ver se o container está em pé. Caso queira entrar nesse container e acessar o MySQL
digite o seguinte comando:

    docker exec -it CONTAINER_ID bash
    
    mysql -u root -p  - SENHA : root

o Container ID conseguimos consultar quando executamos o docker ps.

Agora que já temos o MYSQL rodando local, vamos iniciar a aplicação, no projeto luizalabs procure pela classe
principal chamada LuizaLabsApplication.java, quando encontrar click com botão direito na classe -> Run As -> 
Java Application

Se tudo ocorrer como planejado no console da IDE deve está uma mensagem igual a essa:
        
    Started LuizalabsApplication in 7.197 seconds (JVM running for 7.645)

Nossa aplicação está funcionando =)

____________________________________________________________________________________________________

**ENDPOINTS**

Para começar os testes da aplicação, vamos consultar uma ferramenta chamada Swagger, ele disponibiliza uma
visão geral de todos os endpoints existentes, para acessar entre no endereço:

    http://localhost:8080/swagger-ui.html

Conseguimos consultar todos os endpoints existentes e seus métodos HTTP, além dos models da aplicação.

____________________________________________________________________________________________________

**TESTES**

Vamos começar pela feature de LOGIN, caso queira testar a feature de login, tente acessar qualquer 
endpoint da aplicação sem ter executado o LOGIN na aplicação, a resposta será 403 Forbidden - Access Denied

Primeiro passo precisamos criar um usuário nosso para acessar a aplicação, para isso vamos utilizar o Postman
que basicamente nós ajuda a fazer chamadas HTTP.

Após abrir o postman vá em File -> new -> Request

Vamos chamar o endpoint http://localhost:8080/api/auth/signup com método POST passando o seguinte body:

    Obs: O username deve ser sempre o email e para ajudar nos futuros teste estou printando o USER ID gerado no console
    exemplo: SAVED USER UUID: 02e0e2c8-816d-444f-84e8-f427d8e309d1.

        {
            "username", "email@email.com"
            "password": "123",
            "name": "nome qualquer"
        }

Ele irá retornar o token no Response body:

    User registration successful: { TOKEN }


Vamos copiar esse token e chamar o endpoint http://localhost:8080/api/auth/accountVerification/{token} com método GET 
passando o valor do token na URL, vamos receber a mensagem:

    Account activated successfully

Depois vamos chamar o endpoint http://localhost:8080/api/auth/login com método POST passando no body com as seguintes informações:

    {
        "username": "email@email.com",
        "password": "123"
    }

No Response Body vamos receber o valor do authenticationToken, esse valor é um token JWT, agora vamos precisa dele
para todas as chamadas da aplicação, quando for realizar um request precisamos ir na aba Authorization do Postman
e selecionar o Type: Bearer Token e colocar esse valor do JWT dentro do campo Token.

Agora que estamos logados na aplicação podemos consultar e apagar o usuário chamando o endpoint 
http://localhost:8080/api/customer/{id}, caso queira consultar passe método GET e apagar passe o método DELETE.

Podemos atualizar também com método PATCH chamando o mesmo endpoint acima, só precisamos passar no body os campos
que queremos alterar e alterar o content-type no header da chamada para application/json-patch+json para aceitar
o padrão JSON patch que estamos utilizando, exemplo:
    
    Precisamos mandar os campos nesse padrão o path é o campo que deseja alterar e o value o nome valor.

    [
        { "op":"replace", "path":"/username", "value":"email@email.com" }
    ]


Para consultar a lista de desejo do cliente precisamos chamar o endpoint GET - http://localhost:8080/api/wishlist/{id}
passando o id do cliente.

Para adicionar um item na lista de desejos precisamos chamar o endpoint POST - http://localhost:8080/api/wishlist/add
passando o id do cliente e o id do produto, exemplo:

    {
        "userId": "2fd7abe2-f0cf-4a61-8255-93cef751ffef",
        "productId": "9415a64a-912d-39f2-b72e-223f0bfbf821"
    }