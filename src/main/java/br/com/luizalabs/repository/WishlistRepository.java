package br.com.luizalabs.repository;

import br.com.luizalabs.model.Wishlist;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface WishlistRepository extends JpaRepository<Wishlist, UUID> {

    Optional<List<Wishlist>> findAllByUserId(UUID id);

    Optional<Wishlist> findByUserIdAndProductId(UUID userId, UUID productId);

}

