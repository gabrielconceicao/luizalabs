package br.com.luizalabs.controller;

import br.com.luizalabs.dto.WishlistRequest;
import br.com.luizalabs.dto.WishlistResponse;
import br.com.luizalabs.service.WishlistService;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.UUID;

@RestController
@Api(value = "Wishlist")
@RequestMapping("api/wishlist")
public class WishlistController {

    private static final Logger LOGGER = LoggerFactory.getLogger(WishlistController.class);

    @Autowired
    private WishlistService wishlistService;

    @GetMapping("/{id}")
    public ResponseEntity<List<WishlistResponse>> getWishlist(@PathVariable @NotBlank @Size(min = 36) UUID id) {
        try {
            return new ResponseEntity<>(wishlistService.getWishlistByCustomer(id), HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PostMapping("/add")
    public ResponseEntity<Void> addItem(@RequestBody @Valid WishlistRequest wishlistRequest) throws Exception {
        try {
            wishlistService.addItemWishlist(wishlistRequest);
            return new ResponseEntity<>(HttpStatus.CREATED);

        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }
}
