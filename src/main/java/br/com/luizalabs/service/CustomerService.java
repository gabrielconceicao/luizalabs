package br.com.luizalabs.service;

import br.com.luizalabs.exceptions.LuizaLabsException;
import br.com.luizalabs.model.User;
import br.com.luizalabs.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

@Service
@Transactional
public class CustomerService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    HttpServletRequest httpServletRequest;

    public User getCustomerById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new LuizaLabsException("Customer not found: " + id));
    }

    public void deleteCustomerById(UUID id){
        userRepository.deleteById(id);
    }

    public void updateCustomerInfo(UUID id, JsonPatch jsonPatch) throws ServletException, JsonPatchException, JsonProcessingException {
        User user = getCustomerById(id);
        User userPatched = applyPatch(jsonPatch, user);


      if(!user.getPassword().equals(userPatched.getPassword())){
          userPatched.setPassword(passwordEncoder.encode(userPatched.getPassword()));
      }

      userRepository.save(userPatched);
    }

    private User applyPatch(JsonPatch jsonPatch, User user) throws JsonPatchException, JsonProcessingException {
        JsonNode patched = jsonPatch.apply(objectMapper.convertValue(user, JsonNode.class));
        return objectMapper.treeToValue(patched, User.class);
    }

}
