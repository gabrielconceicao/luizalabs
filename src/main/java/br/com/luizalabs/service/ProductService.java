package br.com.luizalabs.service;

import br.com.luizalabs.exceptions.LuizaLabsException;
import br.com.luizalabs.model.Product;
import br.com.luizalabs.repository.ProductRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {

    @Autowired
    private OkHttpClient okHttpClient;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ProductRepository productRepository;

    public Optional<Product> getProductFromApi(UUID id) throws Exception {
        Request request = new Request.Builder()
                .url("http://challenge-api.luizalabs.com/api/product/" + id + "/")
                .build();

        Response response = okHttpClient.newCall(request).execute();

        if(response.code() != 200)
            throw new LuizaLabsException("Error with integration Product API | status code: " + response.code());

        Product product = objectMapper.readValue(Objects.requireNonNull(response.body()).string(), Product.class);
        return Optional.of(product);
    }

    public Optional<Product> getProductById(UUID id) {
        return productRepository.findProductById(id);
    }
}
