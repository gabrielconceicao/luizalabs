package br.com.luizalabs.service;

import br.com.luizalabs.dto.WishlistRequest;
import br.com.luizalabs.dto.WishlistResponse;
import br.com.luizalabs.exceptions.LuizaLabsException;
import br.com.luizalabs.model.Product;
import br.com.luizalabs.model.User;
import br.com.luizalabs.model.Wishlist;
import br.com.luizalabs.repository.ProductRepository;
import br.com.luizalabs.repository.WishlistRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Transactional
public class WishlistService {

    @Autowired
    private WishlistRepository wishlistRepository;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ModelMapper modelMapper;

    public List<WishlistResponse> getWishlistByCustomer(UUID id) {
        customerService.getCustomerById(id);

        List<Wishlist> wishlist = wishlistRepository.findAllByUserId(id)
                .orElseThrow(() -> new LuizaLabsException("User wishlist not found"));

        return wishlist
                .stream()
                .map(item -> modelMapper.map(item.getProduct(), WishlistResponse.class))
                .collect(Collectors.toList());
    }

    public void addItemWishlist(WishlistRequest wishlistRequest) throws Exception {
        Optional<Wishlist>  w = wishlistRepository.findByUserIdAndProductId(wishlistRequest.getUserId(), wishlistRequest.getProductId());

        if(w.isPresent()){
            throw new LuizaLabsException("Product already exists in wishlist");
        }

        User user = customerService.getCustomerById(wishlistRequest.getUserId());
        Optional<Product> product = productService.getProductById(wishlistRequest.getProductId());

        if(product.isEmpty()){
            product = productService.getProductFromApi(wishlistRequest.getProductId());
            productRepository.save(product.get());
        }

        wishlistRepository.save(new Wishlist(user, product.get(), Instant.now()));
    }

}
