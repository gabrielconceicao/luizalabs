package br.com.luizalabs.exceptions;

public class LuizaLabsException extends RuntimeException {

    public LuizaLabsException(String message, Exception exception) {
        super(message, exception);
    }

    public LuizaLabsException(String message) {
        super(message);
    }
}