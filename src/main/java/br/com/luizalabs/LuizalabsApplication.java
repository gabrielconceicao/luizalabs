package br.com.luizalabs;

import br.com.luizalabs.config.SwaggerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
@Import(SwaggerConfig.class)
public class LuizalabsApplication {

	public static void main(String[] args) {
		SpringApplication.run(LuizalabsApplication.class, args);
	}

}
