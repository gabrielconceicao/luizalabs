package br.com.luizalabs.service;

import br.com.luizalabs.model.User;
import br.com.luizalabs.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

    @InjectMocks
    private CustomerService customerService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private PasswordEncoder passwordEncoder;


    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void getCustomerByIdWithSuccess() {
        when(userRepository.findById(any(UUID.class)))
                .thenReturn(Optional.of(new User(UUID.randomUUID(), "unitTest", "pass", "nameTest", Boolean.TRUE)));

        User user = customerService.getCustomerById(UUID.randomUUID());

        assertEquals("unitTest", user.getUsername());
        assertEquals("nameTest", user.getName());
        assertEquals("pass", user.getPassword());
        assertTrue(user.isEnabled());
    }
}
