package br.com.luizalabs.service;

import br.com.luizalabs.dto.WishlistRequest;
import br.com.luizalabs.dto.WishlistResponse;
import br.com.luizalabs.exceptions.LuizaLabsException;
import br.com.luizalabs.model.Product;
import br.com.luizalabs.model.User;
import br.com.luizalabs.model.Wishlist;
import br.com.luizalabs.repository.ProductRepository;
import br.com.luizalabs.repository.WishlistRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WishlistServiceTest {

    @Mock
    private CustomerService customerService;

    @Mock
    private WishlistRepository wishlistRepository;

    @Mock
    private ModelMapper modelMapper;

    @Mock
    private ProductService productService;

    @Mock
    private ProductRepository productRepository;

    @InjectMocks
    private WishlistService wishlistService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetWishlistByCustomerWithSuccess() {

        List<Wishlist> wishlist = new ArrayList<>();
        wishlist.add(mockWishlistResponse());

        when(wishlistRepository.findAllByUserId(any(UUID.class))).thenReturn(Optional.of(wishlist));
        when(modelMapper.map(any(Product.class), Mockito.eq(WishlistResponse.class)))
                .thenReturn(mockWishlistResponse("id", "title", "image", "100", "review"));

        List<WishlistResponse> wishlistResponse = wishlistService.getWishlistByCustomer(UUID.randomUUID());

        assertNotNull(wishlistResponse);
        assertEquals(1, wishlistResponse.size());
        assertEquals("title", wishlistResponse.get(0).getTitle());
        assertEquals("review", wishlistResponse.get(0).getReview());
    }

    @Test
    public void testGetWishlistByCustomerWithCustomerUnknown() {
        when(wishlistRepository.findAllByUserId(any(UUID.class))).thenReturn(Optional.empty());

        LuizaLabsException thrown = assertThrows(
                LuizaLabsException.class,
                () -> wishlistService.getWishlistByCustomer(UUID.randomUUID())
        );

        assertTrue(thrown.getMessage().contains("User wishlist not found"));
    }

    @Test
    public void testAddItemWishlistWithSuccess() throws Exception {
        when(wishlistRepository.findByUserIdAndProductId(any(UUID.class), any(UUID.class))).thenReturn(Optional.empty());

        User user = new User(UUID.randomUUID(), "usernameTest", "passwordTest", "nameTest",Boolean.TRUE);
        when(customerService.getCustomerById(any(UUID.class))).thenReturn(user);

        Product product = new Product(UUID.randomUUID(), "title", "100", "brand", "image", "review");
        when(productService.getProductById(any(UUID.class))).thenReturn(Optional.of(product));

        wishlistService.addItemWishlist(new WishlistRequest(UUID.randomUUID(), UUID.randomUUID()));

        verify(wishlistRepository, times(1)).save(any(Wishlist.class));
        verify(productRepository, never()).save(any(Product.class));
    }

    @Test
    public void testProductAlreadyExists() {
        when(wishlistRepository.findByUserIdAndProductId(any(UUID.class), any(UUID.class))).thenReturn(Optional.of(mockWishlistResponse()));

        LuizaLabsException thrown = assertThrows(
                LuizaLabsException.class,
                () -> wishlistService.addItemWishlist(new WishlistRequest(UUID.randomUUID(), UUID.randomUUID()))
        );

        assertTrue(thrown.getMessage().contains("Product already exists in wishlist"));
        verify(wishlistRepository, never()).save(any(Wishlist.class));
        verify(productRepository, never()).save(any(Product.class));
    }

    private Wishlist mockWishlistResponse() {
        User user = new User();

        Product product = new Product();
        product.setTitle("productTitle");

        return new Wishlist(user, product, Instant.now());
    }

    private WishlistResponse mockWishlistResponse(String id, String title, String image, String price, String review) {
        return new WishlistResponse(id, title, image, new BigDecimal(price), review);
    }

}
