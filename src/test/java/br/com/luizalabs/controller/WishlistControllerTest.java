package br.com.luizalabs.controller;

import br.com.luizalabs.dto.WishlistResponse;
import br.com.luizalabs.exceptions.LuizaLabsException;
import br.com.luizalabs.service.WishlistService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class WishlistControllerTest {

    @Mock
    private WishlistService wishlistService;

    @InjectMocks
    private WishlistController wishlistController;

    @BeforeEach
    public void init() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    public void testGetWishlistWithSuccess() {
        UUID id = UUID.randomUUID();
        List<WishlistResponse> wishlist = new ArrayList<>();
        wishlist.add(mockWishlistResponse("id", "productTitle", "productImage", "100", "ProductReview"));

        Mockito.when(wishlistService.getWishlistByCustomer(id)).thenReturn(wishlist);
        ResponseEntity<List<WishlistResponse>> response = wishlistController.getWishlist(id);

        assertTrue(response.getStatusCode().toString().contains("200"));
        assertEquals("id", response.getBody().get(0).getId());
        assertEquals("productTitle", response.getBody().get(0).getTitle());
        assertEquals("productImage", response.getBody().get(0).getImage());
        assertEquals("100", response.getBody().get(0).getPrice().toString());
        assertEquals("ProductReview", response.getBody().get(0).getReview());

    }

    @Test
    public void testGetWishlistWithException() {
        UUID id = UUID.randomUUID();
        Mockito.when(wishlistService.getWishlistByCustomer(id)).thenThrow(new LuizaLabsException("Customer not found"));

        ResponseStatusException thrown = assertThrows(
                ResponseStatusException.class,
                () -> wishlistController.getWishlist(id)
        );

        assertTrue(thrown.getMessage().contains("Customer not found"));

    }

    private WishlistResponse mockWishlistResponse(String id, String title, String image, String price, String review) {
        return new WishlistResponse(id, title, image, new BigDecimal(price), review);
    }
}
