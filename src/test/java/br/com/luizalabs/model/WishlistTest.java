package br.com.luizalabs.model;

import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneId;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class WishlistTest {

    @Test
    public void testGetterSetter(){

        UUID uuid = UUID.randomUUID();
        Instant now = Instant.now(Clock.fixed(Instant.ofEpochMilli(0), ZoneId.systemDefault()));

        Wishlist wishlist = new Wishlist();
        wishlist.setId(uuid);
        wishlist.setCreated(now);

        User user = new User();
        user.setName("name");
        user.setUsername("test@email.com");
        user.setPassword("password");
        user.setEnabled(Boolean.FALSE);

        wishlist.setUser(user);

        Product product = new Product();
        product.setBrand("brand");
        product.setPrice("price");
        product.setTitle("title");
        product.setImage("image");
        wishlist.setProduct(product);

        assertEquals(uuid, wishlist.getId());
        assertEquals(now, wishlist.getCreated());

        assertEquals("name", wishlist.getUser().getName());
        assertEquals("test@email.com", wishlist.getUser().getUsername());
        assertEquals("password", wishlist.getUser().getPassword());
        assertFalse(wishlist.getUser().isEnabled());

        assertEquals("brand", wishlist.getProduct().getBrand());
        assertEquals("price", wishlist.getProduct().getPrice());
        assertEquals("title", wishlist.getProduct().getTitle());
        assertEquals("image", wishlist.getProduct().getImage());
    }
}
