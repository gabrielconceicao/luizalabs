package br.com.luizalabs.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class UserTest {

    @Test
    public void testGetterSetter(){
        User user = new User();

        user.setName("name");
        user.setUsername("username@email.com");
        user.setPassword("password");
        user.setEnabled(Boolean.FALSE);

        assertEquals("username@email.com", user.getUsername());
        assertEquals("name", user.getName());
        assertEquals("password", user.getPassword());
        assertFalse(user.isEnabled());
    }

}
